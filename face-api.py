# uvicorn face-api:app --port 5000 --reload

# uvicorn face-api:app --host 192.168.1.4 --port 5000 --reload


from PIL import Image, ImageDraw
import face_recognition
import numpy as np
import pickle

import io
from fastapi import FastAPI, File, Form, UploadFile
from fastapi.encoders import jsonable_encoder
from starlette.middleware.cors import CORSMiddleware

import mysql.connector

# Replace these values with your own database credentials
host = 'localhost'
user = 'root'
password = ''
database = 'face_api'

# Connect to MySQL server
connection = mysql.connector.connect(
    host=host,
    user=user,
    password=password,
    database=database
)

# Create a cursor object to execute SQL queries
cursor = connection.cursor()

app = FastAPI()
app.add_middleware(
    CORSMiddleware, allow_origins=["*"], allow_methods=["*"], allow_headers=["*"]
)

# Load face names and encoding from pickle
known_face_names, known_face_encodings = pickle.load(open('faces.p', 'rb'))

@app.post("/faces_recognition/")
async def faces_recognition(image_upload: UploadFile = File(...)):
    data = await image_upload.read()
    # Load an image
    image = Image.open(io.BytesIO(data))

    # Detect face(s) and encode them
    face_locations = face_recognition.face_locations(np.array(image))
    face_encodings = face_recognition.face_encodings(np.array(image), face_locations)

    face_names = []

    # Recognize face(s)
    for face_encoding, face_location in zip(face_encodings, face_locations):
        matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
        face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
        best_match_index = np.argmin(face_distances)
        name = known_face_names[best_match_index]
        face_names.append(name)

    
    # Define your SELECT query
    select_query = "select f.name  from filename f where f.face_id = (select f2.id from face f2 where f2.name = %s )"
    data_to_insert = (face_names[0],)

    # Execute the SELECT query
    cursor.execute(select_query,data_to_insert)
    # Fetch all the rows from the result set
    rows = cursor.fetchall()

    image_file = []
    # Display the results
    for row in rows:
        print(row)
        image_file.append({"file": row[0]})
    print(image_file)
    return {"image": image_file}