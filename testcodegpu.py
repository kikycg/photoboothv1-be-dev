import torch
import torch.nn as nn
import torch.optim as optim

# Check GPU availability
print("Num GPUs Available: ", torch.cuda.device_count())
print(torch.cuda.is_available())

# Import PyTorch
# import torch

# Check for GPU availability
if torch.cuda.is_available():
    device = torch.device("cuda")
    print("GPU is available")
else:
    device = torch.device("cpu")
    print("GPU is NOT available")

# Create a simple PyTorch computation on GPU
a = torch.tensor([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], device=device)
b = torch.tensor([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], device=device)
c = a * b

# Print the result
print(c)