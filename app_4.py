import face_recognition
import numpy as np
import pickle
import os

# Define a list of known persons with their names and lists of image paths
persons_dir = r"C:\Users\KIKYCG\Desktop\Project\photoBoothAPI\faces"

known_persons_path = [f for f in os.listdir(persons_dir) if os.path.isdir(os.path.join(persons_dir, f))]

data_persons = []

for known_persons in known_persons_path:
    data_images_paths = []  
    file_paths = os.path.join(persons_dir, known_persons)
    for face_persons in [f for f in os.listdir(file_paths) if os.path.isfile(os.path.join(file_paths, f))]:
        data_images_paths.append(os.path.join(file_paths, face_persons))
    data_persons.append({"name": known_persons, "image_paths": data_images_paths})

# Initialize lists for known face encodings and names
known_face_encodings = []
known_face_names = []

# Load known face encodings and names
for person in data_persons:
    person_face_encodings = []
    for image_path in person["image_paths"]:
        image = face_recognition.load_image_file(image_path)
        face_encoding = face_recognition.face_encodings(image)[0]
        person_face_encodings.append(face_encoding)

    # Use the average face encoding for each person
    average_face_encoding = np.mean(person_face_encodings, axis=0)
    known_face_encodings.append(average_face_encoding)
    known_face_names.append(person["name"])

# Dump face names and encoding to pickle
if os.path.exists('faces.p'):
    os.remove('faces.p')
pickle.dump((known_face_names, known_face_encodings), open('faces.p', 'wb'))