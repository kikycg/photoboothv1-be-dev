# Create data for model\\
import torch
import cv2
import os
import dlib
import face_recognition
import numpy as np
import shutil
import sys
import mysql.connector

import torch.nn as nn
import torch.optim as optim
from torchvision import transforms
from torchvision.models import resnet50
from torch.utils.data import DataLoader
from PIL import Image


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"Using device: {device}")


# Replace these values with your own database credentials
host = 'localhost'
user = 'root'
password = ''
database = 'face_api'

# Connect to MySQL server
connection = mysql.connector.connect(
    host=host,
    user=user,
    password=password,
    database=database
)

# Create a cursor object to execute SQL queries
cursor = connection.cursor()

source_image_path = f"18022024/{sys.argv[1]}"

limit_data = 10

# Define a list of known persons with their names and lists of image paths
persons_dir = r"C:\Users\KIKYCG\Desktop\Project\photoBoothAPI\faces_test"

def load_data():
    known_persons_path = [f for f in os.listdir(persons_dir) if os.path.isdir(os.path.join(persons_dir, f))]
    data_persons = []

    for known_persons in known_persons_path:
        data_images_paths = []  
        file_paths = os.path.join(persons_dir, known_persons)
        for face_persons in [f for f in os.listdir(file_paths) if os.path.isfile(os.path.join(file_paths, f))]:
            data_images_paths.append(os.path.join(file_paths, face_persons))
        data_persons.append({"name": known_persons, "image_paths": data_images_paths})
    
    return data_persons


def move_faces_to_folders(image_path, output_folder):
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    detector = dlib.get_frontal_face_detector()
    faces = detector(gray)

    if not faces:
        print("Not detector face")
        return

    for i, face in enumerate(faces):
        x, y, w, h = face.left(), face.top(), face.width(), face.height()
        new_w = int(w * 3)
        new_h = int(h * 3)
        new_x = max(0, x - int((new_w - w) / 2))
        new_y = max(0, y - int((new_h - h) / 2))

        face_img = image[new_y:new_y+new_h, new_x:new_x+new_w]
        cv2.imwrite(os.path.join(output_folder, f'face_{i+1}.jpg'), face_img)

# Define a simple face recognition model using ResNet-50
class FaceRecognitionModel(nn.Module):
    def __init__(self, num_classes):
        super(FaceRecognitionModel, self).__init__()
        self.resnet = resnet50(pretrained=True)
        in_features = self.resnet.fc.in_features
        self.resnet.fc = nn.Linear(in_features, num_classes)

    def forward(self, x):
        return self.resnet(x)

def load_image(image_path):
    image = Image.open(image_path).convert('RGB')
    return image

def preprocess_image(image):
    transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])
    return transform(image)

def matching_face(input_image_path, data_persons, model_path):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Load the pre-trained face recognition model
    num_classes = len(data_persons)
    model = FaceRecognitionModel(num_classes)
    model.load_state_dict(torch.load(model_path, map_location=device))
    model = model.to(device)
    model.eval()

    # Load the input image
    input_image = load_image(input_image_path)
    input_image = preprocess_image(input_image)
    input_image = input_image.unsqueeze(0).to(device)

    # Initialize lists for known face encodings and names
    known_face_encodings = []
    known_face_names = []

    # Load known face encodings and names
    for person in data_persons:
        person_face_encodings = []
        for image_path in person["image_paths"]:
            image = load_image(image_path)
            image = preprocess_image(image)
            image = image.unsqueeze(0).to(device)

            with torch.no_grad():
                face_encoding = model(image)

            person_face_encodings.append(face_encoding)

        # Use the average face encoding for each person
        average_face_encoding = torch.mean(torch.stack(person_face_encodings), dim=0)
        known_face_encodings.append(average_face_encoding)
        known_face_names.append(person["name"])

    # Perform face recognition on the input image
    with torch.no_grad():
        input_encoding = model(input_image)

    # Compare the detected face encoding with the known face encodings
    distances = torch.norm(torch.stack(known_face_encodings) - input_encoding, dim=1)
    min_distance_index = torch.argmin(distances).item()
    min_distance = distances[min_distance_index].item()

    # Set a threshold for face recognition
    threshold = 0.5
    if min_distance < threshold:
        return known_face_names[min_distance_index]
    else:
        return "Unknown"


# def matching_face(input_image_path,data_persons):
#     # Load the input image
#     input_image = face_recognition.load_image_file(input_image_path)

#     # Initialize lists for known face encodings and names
#     known_face_encodings = []
#     known_face_names = []

#     # Load known face encodings and names
#     for person in data_persons:
#         person_face_encodings = []
#         for image_path in person["image_paths"]:
#             image = face_recognition.load_image_file(image_path)
#             face_encoding = face_recognition.face_encodings(image)[0]
#             person_face_encodings.append(face_encoding)

#         # Use the average face encoding for each person
#         average_face_encoding = np.mean(person_face_encodings, axis=0)
#         known_face_encodings.append(average_face_encoding)
#         known_face_names.append(person["name"])

#     # Perform face detection and encoding on the input image
#     face_locations = face_recognition.face_locations(input_image)
#     face_encodings = face_recognition.face_encodings(input_image, face_locations, model="small")

#     # Compare the detected face encodings with the known face encodings
#     face_names = []

#     for face_encoding in face_encodings:
#         matches = face_recognition.compare_faces(known_face_encodings, face_encoding, tolerance=0.5)
#         name = "Unknown"

#         if True in matches:
#             first_match_index = matches.index(True)
#             name = known_face_names[first_match_index]

#         face_names.append(name)

#     return face_names[0]

def move_new_faces(new_file):
    dir = [f for f in os.listdir(persons_dir) if os.path.isdir(os.path.join(persons_dir, f))]
    i_len = len(dir)
    face_folder = os.path.join(persons_dir, f'face_{i_len+1}')
    os.makedirs(face_folder, exist_ok=True)
    shutil.move(new_file, os.path.join(face_folder, f'{sys.argv[1].split(".")[0]}###face_1.jpg'))

    face_name = f'face_{i_len+1}'
    # Insert data into the table
    insert_data_query = "INSERT INTO face(name) VALUES( %s)"
    data_to_insert = (face_name,)
    cursor.execute(insert_data_query, data_to_insert)

    #   Commit the changes
    connection.commit()

    # Retrieve the last inserted ID
    face_id = cursor.lastrowid
    print(f'last inserted ID {face_id}')

    # Insert data into the table
    insert_data_query = "INSERT INTO fileName(name, face_id)VALUES( %s , %s)"
    data_to_insert = (source_image_path,face_id,)
    cursor.execute(insert_data_query, data_to_insert)

    #   Commit the changes
    connection.commit()

    print("new_faces")


def update_face(source_path,file,face_name):
    # Insert data into the table
    insert_data_query = "INSERT INTO fileName(name, face_id)VALUES( %s , (SELECT id FROM face WHERE name = %s ))"
    data_to_insert = (source_image_path,face_name,)
    cursor.execute(insert_data_query, data_to_insert)

    #   Commit the changes
    connection.commit()


    files = [f for f in os.listdir(source_path) if os.path.isfile(os.path.join(source_path, f))]
    i_len = len(files)
    
    if limit_data <= i_len :
        print("Limit update face")
        return
    shutil.move(file, f"{source_path}/{sys.argv[1].split(".")[0]}###face_new_{i_len+1}.jpg")
    print("update_face")

def clear_temp(directory_path):
    for filename in os.listdir(directory_path):
        file_path = os.path.join(directory_path, filename)
        try:
            if os.path.isfile(file_path):
                os.remove(file_path)
        except Exception as e:
            print(f"Error deleting {file_path}: {e}")

if __name__ == "__main__":

    model_path = "model.pth"
    data= load_data()

    print(f"Run File {source_image_path}")
    # image_path = "DSC08141.jpg"
    output_folder = "temp_test"
    os.makedirs(output_folder, exist_ok=True)
    clear_temp(output_folder)
    move_faces_to_folders(source_image_path, output_folder)
    for face_temp in [f for f in os.listdir(output_folder) if os.path.isfile(os.path.join(output_folder, f))]:
        # matching = matching_face(os.path.join(output_folder, face_temp))
        matching = matching_face(os.path.join(output_folder, face_temp), data,model_path)
        

        print(face_temp)
        print(matching)

        if matching == "Unknown":
            move_new_faces(os.path.join(output_folder, face_temp))
        else:
            update_face(os.path.join(persons_dir,matching),os.path.join(output_folder,face_temp),matching)
    clear_temp(output_folder)
    print(f"complete {source_image_path}")
