const { exec } = require('child_process');

// Replace 'your-command-here' with the actual command you want to run
const commandToRun = 'uvicorn face-api:app --port 5000 --reload';

// Execute the command
exec(commandToRun, (error, stdout, stderr) => {
  if (error) {
    console.error(`Error executing command: ${error.message}`);
    return;
  }

  // Process the command output
  console.log('Command output:', stdout);

  // Handle errors in the command execution
  if (stderr) {
    console.error('Command error:', stderr);
  }
});
