import cv2
import face_recognition
import numba
import numpy as np

# Numba decorator for optimizing a function
@numba.jit(nopython=True)
def numba_function(image):
    # Your optimized code here
    return image

# Load a sample image
image_path = "18022024/DSC00017.jpg"
image = cv2.imread(image_path)

# Convert the image to RGB (OpenCV uses BGR by default)
rgb_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# Detect faces in the image
face_locations = face_recognition.face_locations(rgb_image)

# Process each detected face
for face_location in face_locations:
    top, right, bottom, left = face_location

    # Extract the face from the image
    face_image = image[top:bottom, left:right]

    # Call the Numba-optimized function
    processed_face = numba_function(face_image)

    # Display the original and processed faces
    cv2.imshow("Processed Face", processed_face)
    cv2.imshow("Original Face", face_image)
    
    cv2.waitKey(0)

# Release the OpenCV window
cv2.destroyAllWindows()
