const express = require("express");
const helmet = require("helmet");
const xss = require("xss-clean");
const hpp = require("hpp");
const cors = require("cors");
const path = require("path");
const mysql = require('mysql');
const chokidar = require('chokidar');
const multer = require('multer');

const folderPath = './18022024';


const port = process.env.PORT || 3100;
const host = `http://192.168.1.2:${port}/`;
// const host = `http://localhost:${port}/`;
// const host = `https://hilton-etc-heel-buyers.trycloudflare.com/`;

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'face_api'
  });

const app = express();

const watcher = chokidar.watch(folderPath, {
    persistent: true,
  });

  watcher
  .on('add', (path) => {
    path=path.replace("\\", "/")
    console.log(path);
    connection.query(`INSERT IGNORE  INTO image(name) VALUES ('${path}')`, (error, results, fields) => {
        if (error) throw error;
        // console.log('Query results:', results);
      });
  })

// Allow Cross-Origin requests
// app.use(cors({allowedHeaders:['Content-Type', 'Authorization','X-Requested-With','Accept','Origin']}));
app.use(cors());

// Set security HTTP headers
// app.use(helmet());

// // Data sanitization against XSS(clean user input from malicious HTML code)
app.use(xss());

// // Prevent parameter pollution
app.use(hpp());

app.use(express.static('image'),cors())
app.use(express.static('COSJI'),cors())
app.use(express.static('18022024'),cors())
app.use('/image', express.static(path.join(__dirname, 'image')))
app.use('/COSJI', express.static(path.join(__dirname, 'COSJI')))
app.use('/18022024', express.static(path.join(__dirname, '18022024')))

// body-parser
app.use(express.json());

// Set up Multer for handling file uploads
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'image/'); // The folder where uploaded files will be stored
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname); // Unique file name to avoid overwriting
    },
  });
  
  const upload = multer({ storage: storage });
  
  // Define a route for handling file uploads
  app.post('/upload', upload.single('image'), (req, res) => {
    // The file is uploaded successfully, you can do further processing here
    res.json({ message: 'File uploaded successfully' });
  });

app.get('/', (req, res) => {
      res.json({ msg:"Hello" });
  });

  app.get('/photo', (req, res) => {
    const page = req.query.page || 1;
    const limit = req.query.limit || 10;
  
    var startIndex = (page - 1) * limit;
    var files_image = []
    connection.query(`SELECT id, name FROM image ORDER BY id DESC LIMIT ${startIndex}, ${limit};`, (error, results, fields) => {
      if (error) throw error;
      // console.log('Query results:', results);
      results.forEach(file => {
        console.log(file.name);
              files_image.push({file:host+file.name})
            });
      res.json({ image:files_image });
    });
  });

app.listen(port, () => {
    console.log(`Service is running on port ${port}`);
  });